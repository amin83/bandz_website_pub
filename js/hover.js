
socials = {
    "iconsJson":[
        
        { 
            "id":"item1",
            "percent":"10%",
            "compaign":"Facebook Campaign",
            "pictureURL":"images/fcircle.png",
            "margin":"30.5%"
        
        },
        { 
            "id":"item2",
            "percent":"15%",
            "compaign":"Youtube Campaign",
            "pictureURL":"images/ycircle.png",
            "margin":"35.5%"
        
        },
        { 
            "id":"item3",
            "percent":"15%",
            "compaign":"Twitter Campaign",
            "pictureURL":"images/tcircle.png",
            "margin":"40.5%"
        
        },
        { 
            "id":"item4",
            "percent":"25%",
            "compaign":"Telegram Campaign",
            "pictureURL":"images/tecircle.png",
            "margin":"45.5%"
        
        },
        { 
            "id":"item5",
            "percent":"10%",
            "compaign":"Reddit Campaign",
            "pictureURL":"images/rcircle.png",
            "margin":"50%"
        
        },
        { 
            "id":"item6",
            "percent":"15%",
            "compaign":"Articles, Reviews, Publications",
            "pictureURL":"images/doccircle.png",
            "margin":"55%"
        
        },
        { 
            "id":"item7",
            "percent":"10%",
            "compaign":"Translation Campaign",
            "pictureURL":"images/acircle.png",
            "margin":"62%"
        
        },
    ]

}

function showIcon(elem){

    var obj  = socials.iconsJson.find(x=>x.id == elem);


    $("#upper-social1,#upper-social2").attr("src",obj.pictureURL);
    $(".perc-ps").text(obj.percent);
    $(".compaigns").text(obj.compaign);
    $(".hover-div").show();
    $(".hover-div").css("margin-left",obj.margin);
}

function showHoverEffect(elem){

        $("#"+elem).find(".h2-effect").addClass('hover-red');
        $("#"+elem).find(".p-effect").addClass('hover-black');
        $("#"+elem).find(".cd-timeline-img").addClass('hover-shadow');
        $("#"+elem).find(".cd-date").addClass('hover-red');
}
        function hideHoverEffect(elem){
            $(".h2-effect").removeClass('hover-red');
            $(".p-effect").removeClass('hover-black');
            $(".cd-timeline-img").removeClass('hover-shadow');
            $(".cd-date").removeClass('hover-red');
        }   


 $(document).ready(function() {


    $(".g-recaptcha>div").css("margin","0 auto");
    $('.social-icons-bottom').hover(function() {
        var id = $(this).attr('id');
        showIcon(id);

    },function() {
        $(".hover-div").hide();
    });
    $('.cd-timeline-block').hover(function(){
        var id2 = $(this).attr('id');
        showHoverEffect(id2);
    },function(){
        var id2 = $('.cd-timeline-block').attr('id');
        hideHoverEffect(id2);
    })
 });




  var acc = document.getElementsByClassName("accordion");
  for (var i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function () {
          this.classList.toggle("active");
          var panel = this.nextElementSibling;
          if (panel.style.display === "block") {
              panel.style.display = "none";
          } else {
              panel.style.display = "block";
          }
      });
  }



  $(document).ready(function() {
  $(".arrow-points-wrap").click(function () {

    
      if ($(this).find(".dbl-arr-rotate1").attr("src") == "images/three-dots.png") {
          $(this).find(".dbl-arr-rotate1").attr("src", "images/three-dots-vertical.png")
      } else {
          $(this).find(".dbl-arr-rotate1").attr("src", "images/three-dots.png")
      }
  })
});

AOS.init({
    disable: 'mobile'
  });
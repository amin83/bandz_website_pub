

AOS.init();




$(window).scroll(function () {
  var nav = $('#myNav');
  var top = 15;
  if ($(window).scrollTop() >= top) {

    nav.addClass('colored-nav');

  } else {
    nav.removeClass('colored-nav');
  }
});


$('globeBlckBigDiv').delay(9000);


$(".progress").hover(function () {

  $(".tooltip-wrapper").show();

}, function () {
  $(".tooltip-wrapper").hide();
})

/*Mustajab Code*/

var url = "api url here  "

$(function () {

  $.ajax({
    type: 'GET',
    url: url + 'user/getTokenStats',
    dataType: 'json',
    contentType: 'application/json; charset=utf-8',
    success: function (response) {

      console.log(response);
      $(".dollar-numbering").text("$" + response.usdRaised);

      var getval = (response.usdRaisedInNumber / response.hardCapInNumber) * 100;

      getval = getval.toFixed(3);

      var usdRaised = response.usdRaisedInNumber.toFixed(3);

      $(".tooltip-wrapper>.custom-tooltip").text("USD Raised: $" + usdRaised);

      $("#percentage_value").css("width", getval + "%");
    },
    error: function (error) {


      var obj = JSON.parse(error.responseText);
      toastr["error"](obj.message);



    }
  });

});
function keyEntered(event) {
  if (event.keyCode == 13) {
    addToWhiteList();
  }
}

function addToWhiteList() {

  toastr.remove();
  var error = false;
  var name = document.getElementById("whitelist-name").value;
  var email = document.getElementById("whitelist-email").value;
  var ethAddress = document.getElementById("whitelist-ethAddress").value;

  var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;



  if (email == "") {
    error = true;
    toastr["error"]("Enter an email!");
  } else
    if (!regex.test(email)) {
      error = true;
      toastr["error"]("Enter a valid email!");
    }


  if (name == "") {
    error = true;
    toastr["error"]("Enter your name!");
  }



  if (ethAddress == "") {
    error = true;
    toastr["error"]("Enter an ETH Address!");

  }





  if (whiteListCaptcha == undefined) {
    error = true;
    toastr["error"]("Captcha is Required!");
  }


  if (error) {

  } else {


    $('.white-loader').show();
    $("#send-btn").hide();
    $.ajax({
      url: url + 'user/addWhitelistUser',
      type: 'POST',
      data: { email: email, name: name, ethAddress: ethAddress, token: whiteListCaptcha },
      async: false,
      success: function (data) {

        if (data.code == 200) {
          $("#whitelist-email").val("");
          $("#whitelist-name").val("");
          $("#whitelist-ethAddress").val("");
          $('.white-loader').hide();
          $("#send-btn").show();
          toastr["success"]("Your have been successfully whitelisted");

          grecaptcha.reset(recaptcha1);

        } else {
          toastr["error"](data.message);
          $('.white-loader').hide();
          $("#send-btn").show();
          grecaptcha.reset(recaptcha1);
        }

      },
      error: function (error) {

        var obj = JSON.parse(error.responseText);
        toastr["error"](obj.message);
        $('.white-loader').hide();
        grecaptcha.reset(recaptcha1);
        $("#send-btn").show();
      }
    });

  }

}




var ContactCaptcha;
var verifyCallback = function (response) {
  ContactCaptcha = response;
};


var whiteListCaptcha;
var verfiyWhitelistCaptcha = function (response) {
  whiteListCaptcha = response;
}






function validateForm() {

  toastr.remove();

  var subject = "";
  var email = document.getElementById("ur-email").value;
  var msg = document.getElementById("msg").value;
  var check;

  var chech = document.getElementById("test1").check;
  var error = false;

  var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


  if (!regex.test(email)) {
    error = true;
    toastr["error"]("Enter a valid email!");
  }


  if ($('#test1').is(':checked')) {
    check = true;

  } else {

    check = false;
  }

  if (ContactCaptcha == undefined) {
    error = true;
    toastr["error"]("Captcha is Required!");
  }


  if (error) {

  }
  else {


    $('.loader').show();

    $.ajax({
      url: url + 'user/contactus',
      type: 'POST',
      data: { subject: subject, email: email, message: msg, isSub: check, token: ContactCaptcha },
      async: false,
      success: function (data) {

        if (data.code == 200) {
          $("#subj").val("");
          $("#ur-email").val("");
          $("#msg").val("");
          $(".loader").hide();
          toastr["success"](data.message);


          grecaptcha.reset(recaptcha2);


        } else {
          toastr["error"](data.message);
          $(".loader").hide();
          grecaptcha.reset(recaptcha2);
        }

      },
      error: function (error) {

        $(".loader").hide();
        grecaptcha.reset(recaptcha2);
      }
    });



  }
}

$(document).ready(function () {



  [].forEach.call(document.querySelectorAll('img[data-src]'), function (img) {
    img.setAttribute('src', img.getAttribute('data-src'));
    img.onload = function () {
      img.removeAttribute('data-src');
    };
  });

  $(".navbar-nav li a").click(function (event) {
    $(".navbar-collapse").collapse('hide');
  });


  $(".btn-types").click(function () {
    $(".btn-types").removeClass("active-language");
    $(this).addClass("active-language");
    $(".language-types-row").slideDown();
    $(".download-row").slideUp();
    if ($(this).attr("id") == "whitepaper") {
      $('#download-whitepaper').attr("href", "https://drive.google.com/file/d/1q6oKCUcNywqi2ty9kISHGPXmc5V0gL0e/view")


    } else if ($(this).attr("id") == "lightpaper") {
      $('#download-whitepaper').attr("href", "https://drive.google.com/file/d/1BpVJoO1nw1XwvXBkSlRgIWdW0DJXt3jC/view")
    }
  })

  $(".languages").click(function () {
    $(".download-row").slideDown();
  })
});


$(document).ready(function () {

  setTimeout(function () {
    $('body').addClass('loaded');

  }, 3000);

});




